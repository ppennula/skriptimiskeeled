#!/bin/bash
# Autor Priit Pennula
# Bash kontrolltoo 12.04.2016

# Valjumiskoodid
# 1 - Parameeter puudu
# 2 - Parameeter ebasobiv
# 3 - Kataloogi loomine ebaonnestus

# Parameetrite olemasolu kontroll
if [ $# -ne 1 ]
then
    echo "Parameeter puudu; kasutamine: $0 <4kohaline PIN>"
    exit 1
fi

I="-0001"
OTSITAV=$1
FAIL="$OTSITAV/koodid.txt"

# Esimese parameetri sobivuse kontroll
if ! [[ $OTSITAV =~ ^[0-9]{4}$ ]]
then
    echo "Parameeter peab olema neljakohaline margita arv"
    exit 2
fi

# Kontrollime, kas kataloogipuu eksisteerib
test -d $OTSITAV
if [ $# -ne 0 ]
then
    mkdir -p $OTSITAV
    if [ $? -ne 0 ]
    then
        echo "Kataloogi loomine ebaonnestus, ilmselt puuduvad oigused"
        exit 3
    fi
fi

# Kontrollime, kas fail eksisteerib
test -e $FAIL
if [ $? -eq 0 ]
then
    TIMESTAMP=$(date +"%S")
    FAIL="$OTSITAV/koodid$TIMESTAMP.txt"
    echo "Valjundfail (koodid.txt) juba eksisteerib, loon uue faili nimega $FAIL"
fi

# While tsykkel numbri ketramiseks ja faili kirjutamiseks
while [ $I -lt 9999 ]
do
    I=$((I+1))
    if [ $I -eq $OTSITAV ]
    then
        echo "Vaste!" >> $FAIL
        sleep 3
        echo "Jatkan genereerimist"
    fi 
    printf "%04d\n" $I >> $FAIL
    printf "%04d\n" $I
done

echo "Koik voimalikud kombinatsioonid genereeritud, asuvad failis $FAIL"
exit 0
