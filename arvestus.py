# -*- coding: utf-8 -*-
"""Skriptimiskeelte arvestustoo - Python"""

import sys
import os
import urllib
import time

# Kasutatud allikad:
# https://bitbucket.org/ppennula/skriptimiskeeled/src/

# Veakoodid:
# 1 - parameetrid valed
# 2 - sisendfail ligipaasmatu
# 3 - sisendfail tyhi
# 4 - serveriga yhenduse loomine ebaonnestus
# 5 - ebakorrektne sisendfail

# Käsurea parameetrite kontroll
# Pythoni kontrolltoost
if len(sys.argv) != 3:
    print "Valed parameetrid\nKasutamine: python", sys.argv[0], \
        "[serveri nimi/IP] [sisendfail]"
    sys.exit(1)

# Sisendurli korrigeerimine
INPUT_URL = sys.argv[1]
if INPUT_URL.find("http://www.", 0, 11) == 0:
    IN_URL = INPUT_URL
else:
    if INPUT_URL.find("www.", 0, 4) == 0:
        IN_URL = "http://" + sys.argv[1]
    elif INPUT_URL.find("http://", 0, 7) == 0:
        # Kasutatud Stack Overflow kasutaja Eric Leschinski napunaidet
        IN_URL = INPUT_URL[:7] + 'www.' + INPUT_URL[7:]
    else:
        IN_URL = "http://www." + INPUT_URL

# Sisendfaili muutuja defineerimine, ligipääsu ning olemasolu kontroll
# Pythoni kontrolltoost
try:
    INPUT_FILE = open(sys.argv[2], 'r')
except IOError:
    print "Sisendfaili lugemine ebaõnnestus"
    sys.exit(2)

# Sisendfaili mahu kontroll
# Pythoni kontrolltoost
if os.path.getsize(sys.argv[2]) == 0:
    print "Sisendfail on tühi fail"
    sys.exit(3)

# Funktsionaalsus
for line in INPUT_FILE.readlines():
    line = line.strip().split(',')

    try:
        array_test = line[0]
        search_term = line[1]
    except IndexError:
        print "Ebakorrektne sisendfail"
        exit(5)

    # Kontrollime, kas serverile jargnev osa algab kaldkriipsuga
    # ning stripime vajadesel selle kyljest
    if line[0].find('/', 0, 1) == 0:
        url_strip = line[0]
        url_portion = url_strip.lstrip('/')
    else:
        url_portion = line[0]

    # Loome taispika URLi
    url_complete = IN_URL + '/' + url_portion

    # Yhenduse loomine ja yhenduse veakontroll
    try:
        url_connection = urllib.urlopen(url_complete)
    except IOError:
        print "Veebilehe allalaadimine ebaonnestus"
        exit(3)

    # Sikutame veebilehe alla
    url_src = url_connection.read()

    # Kontrollime otsingusona olemasolu
    # Kellaaja inspiratsioon parit Stack Overflow kasutajalt Sean James
    current_time = time.strftime("%Y-%m-%d_%H-%M-%S", time.localtime())
    if url_src.find(search_term) >= 0:
        print url_complete + ',' + search_term + ',' +  current_time + ','\
              + 'OK'
    else:
        print url_complete + ',' + search_term + ',' +  current_time + ','\
              + 'NOK'
