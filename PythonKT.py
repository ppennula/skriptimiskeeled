# -*- coding: utf-8 -*-
"""Pythoni kontrolltöö"""

# Pythoni kontrolltöö ülesanne, mis loeb sisendfailist andmed ning
# kirjutab need ettemääratud kujul väljundfaili.
# Sisendfaili formaat: Jrk.nr   eesnimi perenimi grupp
# Skript ignoreerib ilma järjekorranumbrita ridu.

# Priit Pennula, A21, 18.05.2016

# Veakoodid:
# 1 - valed argumendid
# 2 - sisend- ja väljundfail on sama fail
# 3 - sisendfaili ei eksisteeri või puudub ligipääs
# 4 - sisendfail on tühi
# 5 - väljundfaili loomiseks puuduvad õigused
# 6 - väljundfail eksisteerib, kasutaja ei kirjuta üle
# 7 - vale sisestus ülekirjutamise küsimuse korral

import sys
import re
import string
import random
import os

# Funktsioon. Aluseks võetud Stack Overflow kasutaja "Randy Marsh" lahendus
# Returnib 20-tähemärgise stringi v.a juhul, kui grupinime pikkus >20 tähemärki
def group_random(grp, lth):
    """Suvalise grupi-stringi genereerija"""
    lth = 20 - lth
    return grp + ''.join(random.SystemRandom().choice \
(string.ascii_uppercase + string.ascii_lowercase + string.digits + "-" + "_")\
for _ in range(lth))

# Käsurea parameetrite kontroll
if len(sys.argv) != 3:
    print "Valed parameetrid\nKasutamine: python", sys.argv[0], \
        "[sisendfail] [väljundfail]"
    sys.exit(1)

# Sisend- ja väljundfaili kattuvuse kontroll
if sys.argv[1] == sys.argv[2]:
    print "Sisend- ja väljundfail on üks ja sama fail"
    sys.exit(2)

# Üldmuutujad ja -kontrollid
# Sisendfaili muutuja defineerimine, ligipääsu ning olemasolu kontroll
try:
    INPUT_FILE = open(sys.argv[1], 'r')
except IOError:
    print "Sisendfaili lugemine ebaõnnestus"
    sys.exit(3)

if os.path.getsize(sys.argv[1]) == 0:
    print "Sisendfail on tühi fail"
    sys.exit(4)

# Väljundfaili muutuja defineerimine ja ligipääsu-olemasolu kontroll
if os.path.exists(sys.argv[2]) == True:
    print "Väljundfail eksisteerib, kas kirjutada üle? J/e?"
    USER_IN = raw_input().lower()
    if USER_IN == "" or USER_IN == "j" or USER_IN == "jah":
        try:
            OUTPUT_FILE = open(sys.argv[2], 'w')
        except IOError:
            print "Väljundfaili kirjutamine ebaõnnestus"
            sys.exit(5)
    elif USER_IN == "e" or USER_IN == "ei":
        print "Kasutaja loobus faili üle kirjutamisest"
        sys.exit(6)
    else:
        print "Vale sisestus"
        sys.exit(7)
else:
    try:
        OUTPUT_FILE = open(sys.argv[2], 'w')
    except IOError:
        print "Väljundfaili kirjutamine ebaõnnestus"
        sys.exit(5)

# E-maili aadressi laiend
EMAIL_EXT = "@itcollege.ee"
# Reanumbri loendur vigase sisendfaili tarbeks
ROW_COUNTER = 0

# Skripti põhifunktsionaalsus
for row in INPUT_FILE:
    ROW_COUNTER += 1

    # Regex kontrollimaks, kas rida on tühi (row[0] sisaldab järjenumbrit?)
    if re.search('^[0-9]{1,3}$', row[0]):
        row = row.strip().split()

        # Puuduliku sisendfaili kontroll
        try:
            FIRST_NAME = row[1]
            LAST_NAME = row[2]
            GROUP = row[3]
        except IndexError:
            print "Puuduv väärtus sisendfaili real", ROW_COUNTER
            continue

        # Perenime pikkuse kontroll ja kasutajanime loomine
        if len(LAST_NAME) > 7:
            USER_NAME = FIRST_NAME[:1].lower() + LAST_NAME[:7].lower()
        else:
            USER_NAME = FIRST_NAME[:1].lower() + LAST_NAME.lower()

        # E-maili aadressi loomine
        E_MAIL = FIRST_NAME.lower() + "." + LAST_NAME.lower() + EMAIL_EXT

        # Väljundandmete koondamine
        FORMATTED_DATA = USER_NAME + "," + FIRST_NAME + " " + LAST_NAME + \
"," + E_MAIL + "," + group_random(GROUP, len(GROUP)) + "\n"

        # Väljundandmete kirjutamine faili
        OUTPUT_FILE.write(FORMATTED_DATA)

# Tõmbame otsad kokku
print "Töö edukalt lõpetatud"
INPUT_FILE.close()
OUTPUT_FILE.close()
sys.exit(0)
