#!/bin/bash
# Autor: Priit Pennula (A21)
# Apache

# Valjumiskoodid:
# 1 - Vale arv parameetreid
# 2 - Puuduvad juurkasutaja oigused
# 3 - Sellise kaustaga virtuaalhost juba eksisteerib
# 4 - Sellise nimega virtuaalhost juba eksisteerib
# 5 - Hosts failis juba saidi nimeline kirje olemas
# 6 - Apache paigaldamine ebaonnestus
# 7 - Nimelahenduse loomine ebaonnestus
# 8 - Virtuaalhosti tooleseadmine ebaonnestus

KAUST=/var/www/$1
SAIT=$1

# Parameetrite kontroll
if [ $# -ne 1 ]
then
    echo "Kasutamine: $0 www.veebisait.ee"
    exit 1
fi

# Juurkasutaja oiguste kontroll
if [ $UID -ne 0 ]
then
    echo "Skript tuleb kaivitada juurkasutajana"
    exit 2
fi

# Kontrollib varasema DocumentRoot kausta olemasolu
test -d $KAUST
if [ $? -eq 0 ]
then
    echo "Sellise kaustaga virtuaalhost juba eksisteerib"
    exit 3
fi

# Kontrollib varasema virtuaalhosti konfiguratsiooni olemasolu
test -e /etc/apache2/sites-available/$SAIT
if [ $? -eq 0 ]
then
    echo "Sellise nimega virtuaalhost juba eksisteerib"
    exit 4
fi

# Kontrollib hosts faili sisu
COUNT=$(grep -c " $SAIT$" /etc/hosts)
if [ $COUNT -gt 0 ]
then 
    echo "Sellise hostinimega kirje juba olemas hosts failis"
    exit 5
fi

# Apache installeerituse kontroll
which apache2 > /dev/null
if [ $? -ne 0 ]
then
    echo "Uuendan repositooriumid ja paigaldan Apache"
    apt-get update > /dev/null && apt-get install apache2 -y  > /dev/null
    if [ $? -ne 0 ]
    then
        echo "Apache paigaldamine ebaonnestus"
        exit 6
    fi
else
    echo "Apache on eelnevalt paigaldatud"
fi

# DocumentRoot kausta loomine, index.html kopeerimine ja muutmine
echo "Loon virtuaalhosti tarbeks kausta ja vaikimisi esilehe"
mkdir $KAUST
cp /var/www/html/index.html $KAUST
sed -i "s|<body>|<body>$SAIT|" $KAUST/index.html

# Nimelahenduse loomine
echo "echo 127.0.0.1 $1 >> /etc/hosts" | bash
if [ $? -ne 0 ]
then
    echo "Nimelahenduse loomine ebaonnestus"
    exit 7
else
    echo "Nimelahendus edukalt lisatud"
fi

# Virtuaalhosti konfiguratsioon
echo "Konfigureerin virtuaalhosti"
cp /etc/apache2/sites-available/000-default.conf /etc/apache2/sites-available/$SAIT.conf
sed -i "s|#ServerName www.example.com|ServerName $SAIT|" /etc/apache2/sites-available/$SAIT.conf
sed -i "s|/var/www/html|$KAUST|" /etc/apache2/sites-available/$SAIT.conf 
sed -i "s|error.log|$SAIT-error.log|" /etc/apache2/sites-available/$SAIT.conf 
sed -i "s|access.log|$SAIT-access.log|" /etc/apache2/sites-available/$SAIT.conf 

# Virtuaalhosti tooleseadmine 
a2ensite $SAIT
if [ $? -ne 0 ]
then
    echo "Virtuaalhosti tooleseadmine ebaonnestus"
    exit 8
else
    echo "Virtuaalhost edukalt toole seatud"
fi

# Apache taaskaivitamine
echo "Taaskaivitan Apache"
service apache2 restart
