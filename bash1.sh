#!/bin/bash
# Autor: Priit Pennula (rühm A21)
# Skriptimiskeeled 1. kodutöö (bash)
# Samba

# Väljusmiskoodid:
# 1 - Vale arv parameetreid
# 2 - Puuduvad juurkasutaja õigused
# 3 - Samba install ebaõnnestus
# 4 - Grupi loomine ebaõnnestus
# 5 - Jagatava kausta loomine või õiguste andmine ebaõnnestus
# 6 - Sama nimega share juba eksisteerib
# 7 - Smb.conf seadistamine ebaõnnestus

# Parameetrite kontroll
if [ $# -ne 2 ] && [ $# -ne 3 ]; 
then
	echo "Kasutamine: $0 KAUST GRUPP <JAGATUD KAUST>"
	exit 1
fi

# Muutujate väärtustamine
GRUPP=$2

if [[ $1 = /* ]]
then
	KAUST=$1
else
	KAUST=$(pwd)/$1
fi
 
if [ $# -eq 2 ]
then
	SHARE=$(basename $KAUST)
else
	SHARE=$3
fi

# Juurkasutaja kontroll
if [ $UID -ne 0 ] 
then
	echo "Skript tuleb käivitada juurkasutajana"
	exit 2
fi

# Samba olemasolu kontroll ja install
type smbd > /dev/null 2>&1

if [ $? -ne 0 ]
then
	echo "Uuendan repositooriumid ja installeerin Samba"
	sudo apt-get update > /dev/null 2>&1 && sudo apt-get install samba -y > /dev/null 2>&1
	if [ $? -ne 0 ]
	then
	        echo "Samba installatsioon ebaõnnestus"
		exit 3
	fi
fi

# Grupi olemasolu kontroll
getent group $GRUPP > /dev/null || sudo addgroup $GRUPP > /dev/null
if [ $? -ne 0 ]
then
	echo "Grupi loomine ebaõnnestus"
	exit 4
fi

# Kausta olemasolu kontroll
test -d $KAUST
if [ $? -ne 0 ]
then	
	sudo mkdir -p $KAUST && sudo chown $SUDO_UID $KAUST && sudo chgrp $GRUPP $KAUST
	if [ $? -ne 0 ]
	then
		echo "Jagatava kausta loomine või õiguste muutmine ebaõnnestus"
		exit 5
	fi
fi

# Smb.conf täitmine
KONTROLL=$(grep -c "\[$SHARE]" /etc/samba/smb.conf)
if [[ $KONTROLL -gt 0 ]]
then
	echo "Sama nimega share juba eksisteerib"
	exit 6
fi

sudo cp /etc/samba/smb.conf /etc/samba/smb.conf.old

sudo 	echo "[$SHARE]
	comment=Skripti loodud share
	path=$KAUST
	browsable=yes
	writable=yes
	valid users=@$GRUPP
	force group=$GRUPP
	create mask=0644
	directory mask=0755" >> /etc/samba/smb.conf

testparm -s /etc/samba/smb.conf >> /dev/null

if [ $? -ne 0 ]
then
	echo "Smb.conf seadistamine ebaõnnestus, taastan vaikeseadistuse"
	sudo rm /etc/samba/smb.conf
	sudo mv /etc/samba/smb.conf.old /etc/samba/smb.conf
	exit 7
fi

echo "Reloadin Samba"
sudo /etc/init.d/samba reload
