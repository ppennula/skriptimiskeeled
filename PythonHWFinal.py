"""Pythoni kodune ylesanne"""
# -*- coding: utf-8 -*-

# Script that takes URLs and search terms (in format: http://www.site.tld
# searchterm) from input file, fetches the website source, searches it for
# search term and writes results (in format: URL searchterm JAH/EI) in output
# file. Usage "python PythonHWFinal.py input_file output_file)
# Accepts single-worded search term; URL and search term separator is space
# Priit Pennula A21

# Exit codes:
# 0 - all good
# 1 - wrong number of parameters
# 2 - input file unreadable
# 3 - output file exists, user chose to abort
# 4 - output file inaccessible
# 5 - input file empty
# 6 - empty line in input file
# 7 - input file contents incorrect (either search term or URL missing)
# 8 - unable to connect to URL
# 9 - file overwrite prompt input incorrect

# Importing necessary modules
import sys
import urllib
import os

# Verifying the amount of command line parameters
if len(sys.argv) != 3:
    print "Wrong number of arguments!\nUsage: python", sys.argv[0], \
        "[input file] [output file]"
    sys.exit(1)

# Declaring input- and output file variables and performing some
# error testing
try:
    INPUT_FILE = open(sys.argv[1], 'r')
except IOError:
    print "Unable to read input file"
    sys.exit(2)

if os.path.isfile(sys.argv[2]) == True:
    print "Output file with such name already exists, continue? YES/no?"
    USER_PROMPT = raw_input().lower()
    if USER_PROMPT == "n" or USER_PROMPT == "no":
        print "Chose not to over-write already existing output file, exiting"
        sys.exit(3)
    elif USER_PROMPT == "" or USER_PROMPT == "y" or USER_PROMPT == "yes":
        try:
            OUTPUT_FILE = open(sys.argv[2], 'a')
        except IOError:
            print "Unable to open output file"
            sys.exit(4)
    else:
        print "Wrong yes/no input"
        sys.exit(9)
else:
    try:
        OUTPUT_FILE = open(sys.argv[2], 'a')
    except IOError:
        print "Unable to open output file"
        sys.exit(4)

INPUT_ROWS = INPUT_FILE.readlines()
if len(INPUT_ROWS) == 0:
    print "Input file empty, exiting"
    sys.exit(5)

INPUT_FILE.close()

# Actual functionality of the script
#   Creating a suitable array and extracting the necessary information
for row in INPUT_ROWS:
    row = row.split()
    try:
        url = row[0].strip()
    except IndexError:
        print "Empty line present in input file \
(" + sys.argv[1] + ")"
        sys.exit(6)
    try:
        word = row[1].strip()
    except IndexError:
        print "Missing URL or search term in input file \
(" + sys.argv[1] + ")"
        sys.exit(7)

    # Connecting to the website and fetching source code
    try:
        url_connection = urllib.urlopen(url)
    except IOError:
        print "Unable to connect to", url, \
"Use http://www.site.tld format in input file"
        sys.exit(8)
    url_src = url_connection.read()

    # Determining whether the search term is present in source and writing
    # results in output file
    if url_src.find(word) >= 0:
        out = (url + " " + word + " JAH\n")
        print "Search term", word, "found on", url
    else:
        out = (url + " " + word + " EI\n")
        print "Search term", word, "not found on", url

    OUTPUT_FILE.write(out)

# Cleaning up and exiting in a nice manner
OUTPUT_FILE.close()
sys.exit(0)
